# frozen_string_literal: true

require_relative './lib/team/yaml'

begin
  yaml = Gitlab::Homepage::Team::Yaml.new
  yaml.verify_members!
rescue Gitlab::Homepage::Team::InconsistentTeamError => e
  failure(e.message)
end

begin
  # Using the API, as git.modified_files doesn't work well with shallow clones
  mr_info = gitlab.api.merge_request_changes(gitlab.mr_json['project_id'], gitlab.mr_json['iid'])
  target_branch = mr_info.target_branch
  modified_files = mr_info.to_h["changes"].select { |file| !file['deleted_file'] }.map { |file| file['new_path'] } # rubocop:disable Style/InverseMethods

  unreleased_path = 'data/release_posts/unreleased'
  unreleased_changes = modified_files.collect { |file| file if file.match?(unreleased_path) }.compact

  if unreleased_changes.any?
    # rubocop:disable Lint/UselessAssignment
    msg = <<~MSG
      **Release Post Item Review**

      This merge request modifies files in `#{unreleased_path}` and targets `#{target_branch}`

      If adding items between the 18th and 22nd, work with the RPM per the [Release Post Guidelines](https://about.gitlab.com/handbook/marketing/blog/release-posts/#adding-or-removing-content-blocks-after-the-18th).

      Notifying the release post team @gl-release-post-team for review.

      Relevant changes to `unreleased`: #{unreleased_changes.inspect}
    MSG

    # NOTE(csouthard): We did not anticipate the confusion and/or noise that these messages are generating. Muting them for now or until
    # we can make some enhancements like only messaging within that date window, or as part of the merged pipeline, or targeting a given
    # milestone.
    #
    # markdown(msg)
    # markdown("**If needed, you can retry the [`danger-review` job](#{ENV['CI_JOB_URL']}) that generated this comment.**")

    # rubocop:enable Lint/UselessAssignment
  end
rescue NameError
  # Dont expect gitlab.api.merge_request_changes to work in development
  # The api is equivalent to: Gitlab.client(endpoint: "https://gitlab.com/api/v4", private_token: ENV['GITLAB_BOT_TOKEN'])
end

# vim: filetype=ruby
